# set base image (host OS)
FROM python:3.8

# set the working directory in the container
WORKDIR /src

# copy the dependencies file to the working directory
COPY requirements.txt .
ADD ./src .

# install dependencies
#RUN --mount=type=cache,target=/root/.cache/pip pip install -r requirements.txt
RUN pip install -r requirements.txt

ARG NUM_ROUNDS
ENV NUM_ROUNDS=$NUM_ROUNDS

ARG CLIENT_ID
ENV CLIENT_ID=$CLIENT_ID

ARG USER
ENV USER=$USER

ARG PASS
ENV PASS=$PASS

# command to run on container start
CMD ["sh", "-c", "echo $CLIENT_ID; python main.py"]