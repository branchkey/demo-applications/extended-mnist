import itertools
import random
import torch
import torch.nn as nn
from torch.autograd import Variable
import torch.utils.data


if torch.cuda.is_available():
  dev = "cuda:0"
else:
  dev = "cpu"
device = torch.device(dev)


class Client:
    def __init__(self, client_id, group=None, train_data={'x': [], 'y': []}, eval_data={'x': [], 'y': []}, model=None, model_params=None):
        self._model_temp = model
        if torch.cuda.is_available():
            self.stream = torch.cuda.Stream()
        self.model = self._model_temp(*model_params)
        self.model.to(device)
        self.params = self.model.get_params()
        self.id = client_id
        self.group = group
        self.train_data = train_data['x']
        self.train_data_labels = train_data['y']
        self.eval_data = eval_data['x']
        self.eval_data_labels = eval_data['y']
        self.batch_size = 0

    def evaluate(self, test_loader):
        with torch.no_grad():
            error = nn.CrossEntropyLoss().to(device)
            correct = 0
            running_acc = 0
            running_loss = 0
            for batch_idx, (X_batch, y_batch) in enumerate(test_loader):
                var_X_batch = Variable(X_batch).float()
                var_y_batch = Variable(y_batch)
                var_X_batch = var_X_batch.to(device)
                var_y_batch = var_y_batch.to(device)

                output = self.model(var_X_batch)
                loss = error(output, var_y_batch)

                # Total correct predictions
                predicted = self.model.prediction(output)
                correct += self.model.eval_correct(predicted, var_y_batch)
                # print(correct)
                acc = float(correct * 100) / float(test_loader.batch_size * (batch_idx + 1))
                running_acc += acc
                running_loss += loss.data
        del var_y_batch, var_X_batch, error
        torch.cuda.empty_cache()
        running_acc = running_acc / (batch_idx+1)
        running_loss = running_loss / (batch_idx+1)
        print('Client : {}, Test \tLoss: {:.6f}\t Accuracy:{:.3f}%'.format(
            self.id, running_loss, running_acc))
        self.params = self.model.get_params()
        return {"accuracy": running_acc, 'loss': running_loss}

    def fit(self, train_loader, num_epochs):
        optimizer = torch.optim.Adam(self.model.parameters(), lr=self.model.lr)
        error = nn.CrossEntropyLoss().to(device)
        for epoch in range(num_epochs):
            correct = 0
            list_len = len(train_loader) - 1
            for batch_idx, (X_batch, y_batch) in enumerate(train_loader):
                var_X_batch = Variable(X_batch).float()
                var_y_batch = Variable(y_batch)
                var_X_batch = var_X_batch.to(device)
                var_y_batch = var_y_batch.to(device)

                optimizer.zero_grad()
                output = self.model(var_X_batch)
                loss = error(output, var_y_batch)

                if batch_idx == list_len:
                    loss.backward()
                else:
                    loss.backward(retain_graph=True)
                optimizer.step()

                _, predicted = output.max(dim=1)
                correct += self.model.eval_correct(predicted, var_y_batch)

                print('Client : {}, Epoch : {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}\t Accuracy:{:.3f}%'.format(
                    self.id, epoch, batch_idx * len(X_batch), len(train_loader.dataset),
                           100. * batch_idx / len(train_loader), loss.data,
                           float(correct * 100) / float(self.batch_size * (batch_idx + 1))))
        del var_y_batch
        del var_X_batch
        del error
        torch.cuda.empty_cache()

    def train(self, num_epochs=1, batch_size=10, minibatch=None, results=list, lock=None):
        self.batch_size = batch_size

        if minibatch is None:
            indices = random.sample(range(0, len(self.train_data)), min(len(self.train_data), batch_size))
        else:
            indices = random.sample(range(0, len(self.train_data)), min(len(self.train_data), minibatch*batch_size))
            num_epochs = 1

        train_x = list(itertools.compress(self.train_data, indices))
        train_y = list(itertools.compress(self.train_data_labels, indices))
        torch_X_train = self.model.process_x(train_x)
        torch_y_train = self.model.process_y(train_y)
        train = torch.utils.data.TensorDataset(torch_X_train, torch_y_train)
        train_loader = torch.utils.data.DataLoader(train, batch_size=self.batch_size, shuffle=True)

        self.fit(train_loader, num_epochs)
        torch.cuda.empty_cache()
        self.params = self.model.get_params()
        if results is not None:
            if lock is not None:
                lock.acquire()
                results.append([len(self.train_data_labels), self.params])
                lock.release()
                return
            else:
                results.append([len(self.train_data_labels), self.params])
                return

        return len(self.train_data_labels), self.params

    def test(self, set_to_use='test', results=list, lock=None):
        assert set_to_use in ['train', 'test', 'val']
        if set_to_use == 'train':
            torch_X_test = self.model.process_x(self.train_data)
            torch_y_test = self.model.process_y(self.train_data_labels)
            torch_X_test.to(device)
            torch_y_test.to(device)

        elif set_to_use == 'test' or set_to_use == 'val':
            torch_X_test = self.model.process_x(self.eval_data)
            torch_y_test = self.model.process_y(self.eval_data_labels)
            torch_X_test.to(device)
            torch_y_test.to(device)

            # data = self.eval_data
        test = torch.utils.data.TensorDataset(torch_X_test, torch_y_test)
        test_loader = torch.utils.data.DataLoader(test, batch_size=len(test), shuffle=False)
        torch.cuda.empty_cache()
        results.append([self.id, self.evaluate(test_loader)])
        return True

    def set_params(self, new_params):
        # t = torch.from_numpy(new_params)
        self.params = self.model.set_params(new_params)

    def get_params(self):
        return self.model.get_params()

    @property
    def num_test_samples(self):
        """Number of test samples for this client.
        Return:
            int: Number of test samples for this client
        """
        if self.eval_data is None:
            return 0
        return len(self.eval_data_labels)

    @property
    def num_train_samples(self):
        """Number of train samples for this client.
        Return:
            int: Number of train samples for this client
        """
        if self.train_data is None:
            return 0
        return len(self.train_data_labels)

    @property
    def num_samples(self):
        """Number samples for this client.
        Return:
            int: Number of samples for this client
        """
        return self.num_test_samples + self.num_train_samples
