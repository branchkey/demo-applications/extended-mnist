import os
import shutil
import zipfile

import numpy as np
import requests
from branchkey.client import Client as BKC
from termcolor import cprint

from client import Client
from models import cnn
from utils.model_utils import read_data

credentials = {"leaf_name": "",
               "leaf_id": "",
               "leaf_session_token": "",
               "performance_analyser_username": "hello",
               "performance_analyser_password": "world",
               "response_host":"response.branchkey.com"}

host = "http://api.branchkey.com"

print_green = lambda x: cprint(x, 'green')
print_red = lambda x: cprint(x, 'red')
print_blue = lambda x: cprint(x, 'blue')
print_yellow = lambda x: cprint(x, 'yellow')


def create_clients(users, groups, train_data, test_data, model, model_params):
    if len(groups) == 0:
        groups = [[] for _ in users]
    return [Client(u, g, train_data[u], test_data[u], model.__class__, model_params) for u, g in zip(users, groups)]


def setup_clients(dataset_directory, model=None, use_val_set=False, model_params=None, client_id=None):
    """Instantiates clients based on given train and test data directories.
    Return:
        all_clients: list of Client objects.
    """
    eval_set = 'test' if not use_val_set else 'val'
    train_data_dir = os.path.join(dataset_directory, 'train')
    test_data_dir = os.path.join(dataset_directory, eval_set)

    users, groups, train_data, test_data = read_data(train_data_dir, test_data_dir, client_id)

    return create_clients(users, groups, train_data, test_data, model, model_params)


if __name__ == "__main__":
    results = []
    num_rounds = os.getenv("NUM_ROUNDS", 100)
    client_id = os.getenv("CLIENT_ID", 5)
    num_rounds = int(str(num_rounds))
    client_id = int(str(client_id))
    model_params = os.getenv("MODEL_PARAMS", (0.001, 62))

    credentials["leaf_name"] = os.getenv("BK_LEAF_NAME", "UNSET")
    credentials["leaf_id"] = os.getenv("BK_LEAF_ID", "UNSET")
    credentials["leaf_session_token"] = os.getenv("BK_LEAF_TOKEN", "UNSET")

    c = BKC(credentials, host, ssl=True, wait_for_run=True, run_check_interval_s=15)
    print("Logged in")

    print("Downloading dataset for client {}".format(client_id))
    url = "https://emnist.s3.eu-central-1.amazonaws.com/iid/{}.zip".format(client_id)
    r = requests.get(url, stream=True)
    if not os.path.isdir("../data"): os.makedirs("../data")

    if not os.path.isfile("../data/{}.zip".format(client_id)):
        with open("../data/{}.zip".format(client_id), 'wb') as fd:
            for chunk in r.iter_content(chunk_size=(1024 * 1024)):
                fd.write(chunk)
    print("Files in data directory: {}".format(os.listdir("../data")))
    with zipfile.ZipFile("../data/{}.zip".format(client_id), 'r') as zip_ref:
        zip_ref.extractall("../data/")
    for file in os.listdir("../data"):
        if os.path.isfile("../data/" + file):
            if file.startswith("train"):
                os.rename("../data/" + file, "../data/train.json")
                if not os.path.isdir("../data/train"): os.mkdir("../data/train")
                if os.path.isfile("../data/train/train.json"): os.remove("../data/train/train.json")
                shutil.move("../data/train.json", "../data/train/")
            if file.startswith("test"):
                os.rename("../data/" + file, "../data/test.json")
                if not os.path.isdir("../data/test"): os.mkdir("../data/test")
                if os.path.isfile("../data/test/test.json"): os.remove("../data/test/test.json")
                shutil.move("../data/test.json", "../data/test/")
    client_model = cnn.ClientModel(*model_params)
    clients = setup_clients("../data",
                            model=client_model, model_params=model_params, client_id=client_id)
    for i in range(num_rounds):
        clients[0].train(num_epochs=10, batch_size=5, minibatch=5, results=results)
        update = c.convert_pytorch_numpy(clients[0].get_params(), weighting=5 * 5)
        with open("test.npy", "wb") as f:
            np.save(f, update)
        try:
            print_green("Parameters uploaded: {}".format(c.file_upload("test.npy")))
        except Exception as e:
            print_red("Upload Error {}".format(e))

        if not c.queue.empty():
            m = c.queue.get(block=False)
            try:
                print_green("File {} Downloaded successfully: {}".format(m, c.file_download(m)))
            except Exception as e:
                print_red("Error with file download: {}".format(e))

            t = np.load("./aggregated_output/" + m, allow_pickle=True)
            clients[0].set_params(t)
            print_green("Local parameter update loaded successfully")
            os.remove("./aggregated_output/" + m)
        clients[0].test(results=results)

        print("Round {}".format(i + 1))
