import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import copy


class ClientModel(nn.Module):
    def __init__(self, lr, output_size):
        self.lr = lr
        self.output_size = output_size
        super(ClientModel, self).__init__()
        self.conv1 = nn.Conv2d(1, 32, kernel_size=5)
        self.conv2 = nn.Conv2d(32, 32, kernel_size=5)
        self.conv3 = nn.Conv2d(32, 64, kernel_size=5)
        self.fc1 = nn.Linear(3 * 3 * 64, 256)
        self.fc2 = nn.Linear(256, self.output_size)
        self.use_dropout = False

    def forward(self, x):
        x = self.conv1(x)
        x = F.relu(x)
        # x = F.dropout(x, p=0.5, training=self.training)
        x = self.conv2(x)
        x = F.max_pool2d(x, 2)
        x = F.relu(x)
        if self.use_dropout:
            x = F.dropout(x, p=0.5, training=self.training)
        x = self.conv3(x)
        x = F.max_pool2d(x, 2)
        x = F.relu(x)
        if self.use_dropout:
            x = F.dropout(x, p=0.5, training=self.training)
        x = x.view(-1, 3*3*64)
        x = self.fc1(x)
        x = F.relu(x)
        if self.use_dropout:
            x = F.dropout(x, training=self.training)
        x = self.fc2(x)
        x = F.log_softmax(x, dim=1)
        return x

    def process_x(self, raw_x_batch):
        raw_x_batch = np.asarray(raw_x_batch, dtype=np.float32)
        torch_X_train = torch.from_numpy(raw_x_batch).type(torch.LongTensor)
        torch_X_train = torch_X_train.view(-1, 1, 28, 28).float()
        return torch_X_train

    def process_y(self, raw_y_batch):
        raw_y_batch = np.asarray(raw_y_batch, dtype=np.int)
        return torch.from_numpy(raw_y_batch).type(torch.LongTensor)

    def get_params(self):
        params = []
        for name, param in self.named_parameters():
            params.append([name, copy.deepcopy(param).cpu()])
        device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        self.to(device)
        return params

    def set_params(self, new_params):
        for param, new_param in zip(self.named_parameters(), new_params):
            param[1].data = torch.from_numpy(new_param)
        device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        self.to(device)
        return self.get_params()

    def prediction(self, output):
        return torch.max(output.data, 1)[1]

    def eval_correct(self, predicted, var_y_batch):
        return (predicted == var_y_batch).sum()

    def num_trainable(self):
        return sum(p.numel() for p in self.parameters() if p.requires_grad)
