# BranchKey Demo Application - Federated Extended MNIST

![BK_logo](docs/branchkeytext.png)

------------------------------

BranchKey offers [Federated Learning](https://en.wikipedia.org/wiki/Federated_learning) as-a-service. This repository offers a technical
demonstration for one of the available aggregation techniques; Federated averaging.

This requires the [installation of the BranchKey client
](#install-branchkey-client) and verified user accounts for each learner, if you do not yet have these login details please contact us: [info@branchkey.com](mailto:info@branchkey.com)

> Please [join our discourse server](https://discourse.branchkey.com/) for support or technical questions

## Table of Contents
- [Quickstart](#quickstart)
- [About the demo](#about-the-demo)
- [How does Aggregation work?](#how-does-aggregating-work)
  - [The short version](#the-short-version) 
  - [The long version](#the-long-version) 
- [Setup](#setup)
  - [Local from source](#local)
    - [Anaconda](#conda)
    - [pip installer](#pip-installer)
    - [Build Docker image](#local-build-docker)
  - [Pre-built images](#pre-built-image)
    - [Single Docker container](#docker-run-single-container)
    - [**Recommended:** Docker-compose multi-agent](#docker-compose-run-multiple-simulated-agents)
- [Feedback](#feedback)
## Quickstart
> This method requires [docker-compose](https://docs.docker.com/compose/)

Open a terminal in the root of this project
```shell script
cd ./extended-mnist/
```

Pull the latest images from the BranchKey container registry
```shell script
docker-compose pull
```
Start the 4 simultaneous learners
```shell script
docker-compose up
```

### About the Demo

------------------------------

This demo uses the [Extended MNIST 62 Class image dataset](https://arxiv.org/abs/1702.05373) with numbers 0-9, letters a-z, and capital case A-Z.

![EMNIST](./docs/62EMNIST.png)

The architecture is a generic 3-layer convolutional neural network written using the [PyTorch library](https://pytorch.org/), with some max pooling layers, rectified linear activation units, and
non-optimised hyperparameters. This architecture is intended for demo only.

This program will demonstrate launching multiple machine learners, each with different
datasets. Every agent has the same model architecture and fits the model to their local dataset
each update round. At the end of each round, the learners send the modified models parameters
via the BranchKey client to a central server, where a federated average solution is produced. This
subsequently gets returned to each agent in the pool, the central/globally calculated model
is loaded into each agent, and training continues on the local dataset.

## How does aggregating work?
### The short version
> If you're looking to understand a little more, skip this section, grab a coffee, and check out the [long version](#the-long-version).
------------------------------

Each machine learner sees a partial private dataset, this data is used
to fit a model, the models from multiple distributed agents learning from
private datasets are sent to BranchKey, where BranchKey technology
merges, or aggregates, them together as if the whole dataset
has been used by a single central learner in the training process.

### The long version

------------------------------

The processes you are trying to model in a distributed fashion are not independent of each other.
Using this handwriting dataset as an example, your handwriting and your friends could be quite different from each other.

The underlying process of handwriting we are trying to model is the globally agreed shape of the letter (x), 
although no one has **the** correct way of writing each letter, independently we can write letter to a
close enough approximation to understand each other.
It is difficult to say if there is a ground truth way of writing (x), the size, shape, font, etc
vary very slightly, but we as humans recognise them as the symbol they are trying to represent, (x), because we learn from
multiple texts and many people over our lives.

How would I tell you what a new letter looks like? I would probably need to write a few samples, and send them to you.

Take for a moment, that you, and I may have learned to read and write in different education systems, 
with sometimes a different language, and yet I hope you can interpret this text with ease (at least the letters).

That in itself is an amazing achievement, there is some globally agreed consensus about how letters should look
that no one single person (_fact check_) invented, we as individual collectives of people from different countries, cultures 
and backgrounds have come to collaborate.

#### Okay, now tell me about how BranchKey does it with machines

Firstly, each individual learner fits their model to a private dataset. 
Then these individual models are each sent to the BranchKey server.

![step1](docs/step1.jpg)

Next, the BranchKey server aggregates these updates to form a single model, that 
has been learned from consensus rather than by a ground truth.

![step2](docs/step2.jpg)

Then, the aggregated model is returned to all agents in the pool, even those who did
not contribute can benefit and use the model or inference.

![step3](docs/step3.jpg)

And finally, the process is repeated continuously. Using this updated model,
each individual learner adjusts their local parameters to the private dataset, 
this adjustment is then sent to the BranchKey aggregator, and the cycle repeats.

![step4](docs/step4.jpg)


Similar to how humans communicate via informational representation, BranchKey 
aggregation technology allows for machines to learn from private data. Extract
only the information required for modelling. And leave the remaining data safe on the
edge devices, reducing security risks from handling user data in transit,
bandwidth required for transmission, and centralised compute costs in datacentres
(including their high fees for data transfers).

# Setup

------------------------------

There multiple methods to run this demo:

1. Run local source
   1. [Using Anaconda Package Manager](#conda)
   2. [Using pip install](#pip-installer)
   3. [Build Docker image from source](#local-build-docker)
2. Run using pre-built image
    1. [Single Docker container](#docker-run-single-container)
    2. [**Recommended**: Docker-compose multiple agents](#docker-compose-run-multiple-simulated-agents)

Locally building and running multiple instances gives you more control 
and understanding of what is happening. 
The Docker implementations is a quick and easy
way to demonstrate the functionality of the API.

> ### Optional environmental variables (default values shown):
> 
> `$NUM_ROUNDS=100`
> 
> `$CLIENT_ID=0`
> 
> `$BK_USER=emnist-demo-a1cb487e90374eccbfdc0243a4b277f1`
> 
> `$BK_PASS=b310b8bedc864fc69e4c3559e8740a12`
> 
> `$BK_GROUP=emnist-demos`
> 
> `$BK_CUST=demo`

## Local

------------------------------
> ### REQUIRED: BranchKey client
> ```shell script
> pip install branchkey --extra-index-url https://__token__:v1PetnXjCx3wnic1rndK@gitlab.com/api/v4/projects/22109672/packages/pypi/simple --force-reinstall
> ```
### Conda
Add pytorch channel for Conda
```shell script
conda config --env --add channels pytorch
```
Install dependencies from file
```shell script
conda install --file requirements.txt
```
Run demo
```shell script
python main.py
```

### Pip installer
Install dependencies from file
```shell script
pip install -r requirements.txt
```
Run demo
```shell script
python main.py
```

### Local build Docker
```shell script
docker build -t emnist-demo .
```

```shell script
docker run --env NUM_ROUNDS=100 --env CLIENT_ID=1 --env BK_USER=demo-user-1234 --env BK_USER=password emnist-demo
```


## Pre-built image

------------------------------

> ### REQUIRED: Login
> ```shell script
> docker login registry.gitlab.com -u demo-application -p y-sb6mQnPKFDAuoSbae7
> ```
### Docker: Run single container
> This method requires [docker engine](https://docs.docker.com/engine/install/)

Running a docker container with a single learner. View [optional environmental variables](#optional-environmental-variables-default-values-shown)
to modify image.
```shell script
docker run --env NUM_ROUNDS=100 --env CLIENT_ID=0 --env BK_USER=test-user-7a48068029e34c059201e73b41d27fad --env BK_PASS=7a48068029e34c059201e73b41d27fad --name emnist-demo registry.gitlab.com/branchkey/demo-applications/extended-mnist
```



### Docker Compose: Run multiple simulated agents
> This method requires [docker-compose](https://docs.docker.com/compose/)

Pull the latest images from the BranchKey container registry
```shell script
docker-compose pull
```
Start the 4 simultaneous learners
```shell script
docker-compose up
```

# Feedback and support
We appreciate any and all feedback on the usability, docs, feature requests, or most
importantly support. Please
email us at [info@branchkey.com](mailto:info@branchkey.com) or reach out on our
[community discourse](https://discourse.branchkey.com/)



